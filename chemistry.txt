examples
--------------

O₂

H-CH₂CH₃

CH₄ + O₂ → CO₂ + H₂O

H─H → H˙ + H˙

H─H → H⁻ + H⁺

Na⁺


the characters
-----------------

⁺	superscript plus sign

⁻	superscript minus

−

⌬

⏣	

⚛

U+21CC chemical equilibrium

U+21CB chemical equilibrium


bonds
------------

kludges:

-	single bond (single bar; hyphen)
=	double bond (double bar; equal)
≡	triple bond (triple bar; NAME: identical to)


units
------------

kcal⋅mol⁻¹


thermodynamic quantities
------------------------

DH°

ΔH°
