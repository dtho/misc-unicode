dots
----------------------

·
middle dot
00B7 &#x00B7; middot &middot;

˙	
DOT ABOVE

⋅
DOT OPERATOR

BULLET (closed circle)
•	


circles
----------------------

○
circle (open)
	25Cb &cir;

white circle
○

◙ 	INVERSE WHITE CIRCLE

large circle
◯

xcirc 25EF
◯


misc. dot-related characters
----------------------------

colon:
	003A &x003A; colon &colon;

dieresis (two dots above):
	00A8 &die;
	- see also umlaut


double baseline dot (en leader) (two horizontal dots)
	2025 &nldr;

… horizontal ellipsis (three horizontal dots)
	2026 &hellip;

	- see also em leader, vertical ellipsis

em leader (three horizontal dots)
	2026 &mldr;


period ('full stop')
	002E &#x002E; period &period;

ring (circle above)
	02DA &ring;

ring operator
∘

umlaut (two dots above)
	00A8 &uml;

vertical ellipsis
	22EE &vellip;

white bullet
◦



others:
00A8 Dot

20DC DotDot
⃜

tdot 20DB
⃛

compfn 2218
∘

there4 2234
∴

becaus 2235
∵

sdot 22C5
⋅

